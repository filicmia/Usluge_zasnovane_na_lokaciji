setwd("~/Documents/Rijeka/LBS/Usluge_zasnovane_na_lokaciji/V3")
setwd("./Oregon data files")

library(maptools)
library(RColorBrewer)
library(classInt)
library(maps)
#Zadatak: učitati bazu orotl.shp
orotl.shp = readShapePoly("orotl.shp",proj4string = CRS("+proj=longlat"))
#pomoću nje kreirati digitalnu kartu regija države oregon.
plot(orotl.shp)

title(main = "Oregon")

#Zadatak: učitati koordinate stanica za očitavnje klimatskih promjena
#iz datoteke orstationc.csv (stupci s imenom x i y)
stations = read.csv("orstationc.csv")

summary(stations)
points(stations$lon,stations$lat,col = "blue",cex=0.6)

#Zadatak: Obojajte točke/krugove koji predstavljaju pojedinu baznu stanicu nijansom plave boje
#koja odgovara vrijednosti varijavle tann iz datoteke orstationc.csv.
#Broj različitih nijansi plave boje neka je 8.
plotvar = stations$tann
ncol = 8

colPal = brewer.pal(ncol,"Blues")#"PuOr"
classes = classIntervals(plotvar, ncol,style = "equal")
colcode = findColours(classes,colPal)

names(attr(colcode,"table"))

points(stations$lat,stations$lon,col = colcode,cex=0.6)
legend(x=-117,y=42.9,legend = names(attr(colcode,"table")),fill = colcode,cex = 0.25)

