[![N|Solid](http://www.riteh.uniri.hr/static/dkms/img/logo-st-bluehalo.hr.png)](https://riteh.hr)

# Predmet: Usluge zasnovane na lokaciji

Prateći sadržaju uz predavanja i vježbe izbornog predmeta Usluge zasnovane na lokaciji, Tehnički fakultet Sučilišta u Rijeci, ak. 2017/2018.
Sadržaj će biti nadopunjen/mijenjan kako semestar odmiče.

Poveznica na predavanja i slideove s vježbi: Dropbox

Profesor: Renato Filjar \\
Asistent: Mia Filić, filicmia[at]gmail[dot]com

## Obavijesti
Dan je predložak Projektne specifikacije za seminarski projekt. Vaš ne mora biti na engleskom jeziku, ali može.

### Zadaće - bodovi
U pdf-u zadace_rezultati.pdf

Napomena: 
Ukupni broj bodova je 100 i pridonosi ukupnoj ocjeni s 30%. Dakle, ukoliko imate 100 bodova, dobivate maksimalni broj bodova iz zadaća (30), ukoliko imate x bodova, dobivate x*0.3 bodova iz zadaća od mogućih 30.

Za sva pitanja u vezi rezultata zadaća stojim na raspolaganju.

## Korisne poveznice
```sh

```